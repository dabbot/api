import { createConnection, ConnectionOptions } from 'typeorm';
import DatabaseOptions from './Configuration/DatabaseOptions';

export const createDatabaseConnection = async () => {
  try {
    createConnection(DatabaseOptions as ConnectionOptions)
  } catch(e) {
    console.error(e);
    return e;
  }
  
}