import 'reflect-metadata';
import { Container } from 'typedi';
import { createKoaServer, useContainer } from 'routing-controllers';
import { useContainer as ormUseContainer } from 'typeorm';
import { createDatabaseConnection } from './createDatabase';
import ServiceMapping from './Configuration/ServiceMapping';

(async () => {
    await useContainer(Container);
    await ormUseContainer(Container);
    await createDatabaseConnection();
    const app = createKoaServer({ routePrefix: '/api', controllers: ServiceMapping });
    app.listen(3000);
})()
