import { Repository } from 'typeorm';
import { InternalServerError } from 'routing-controllers';

export abstract class AbstractRepository<T> extends Repository<T> {
  public async findAll(): Promise<T[]> {
    try {
      return await this.find();
    } catch(e) {
      throw new InternalServerError(e);
    }
  }
}
