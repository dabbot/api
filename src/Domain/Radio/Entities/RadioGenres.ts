import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('radio_genres')
export class RadioGenres {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 32, nullable: false, unique: true })
  name: string;
  
}
