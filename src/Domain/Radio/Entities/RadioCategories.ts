import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('radio_categories')
export class RadioCategories {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 32, nullable: false, unique: true })
  name: string;
  
}
