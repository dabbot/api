import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, ManyToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { RadioCategories } from './RadioCategories';
import { RadioGenres } from './RadioGenres';

@Entity('radio')
export class Radio {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 128, nullable: false })
  name: string;

  @Column({ length: 8, nullable: false })
  locale: string;
  
  @Column({ length: 128, nullable: false, unique: true })
  url: string;

  @Column('text', { nullable: false })
  track_data: string;

  @ManyToOne(type => RadioCategories, category => category.id)
  @JoinColumn({ name: 'category_id', referencedColumnName: 'id' })
  category_id: RadioCategories

  @ManyToMany(type => RadioGenres)
  @JoinColumn({ name: 'genre_ids', referencedColumnName: 'id' })
  genre_ids: RadioGenres[]

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date

}
