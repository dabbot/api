import { Service } from 'typedi';
import { GuildSetting } from '../Entities/GuildSetting';
import { EntityRepository } from 'typeorm';
import { InternalServerError } from 'routing-controllers';
import { AbstractRepository } from 'Abstract/AbstractRepository';

@Service()
@EntityRepository(GuildSetting)
export default class GuildSettingRepository extends AbstractRepository<GuildSetting> {

  public async findGuildSetting(id: GuildSetting['guild_id']): Promise<GuildSetting> {
    try {
      const result = await this.findOne();
      if(result instanceof GuildSetting) return result;
      else throw new InternalServerError('Error getting guild setting');
    } catch(e) {
      throw new InternalServerError(e);
    }
  }

}
