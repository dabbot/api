import { Service } from 'typedi';
import { Guild } from '../Entities/Guild';
import { EntityRepository } from 'typeorm';
import { AbstractRepository } from 'Abstract/AbstractRepository';

@Service()
@EntityRepository(Guild)
export default class GuildRepository extends AbstractRepository<Guild> {}
