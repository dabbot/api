import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('guild_settings')
export class GuildSetting {

  @PrimaryGeneratedColumn()
  id: number;

  @Column('bigint')
  guild_id: number;

  @Column({ length: 10, nullable: true })
  prefix: string;
  
}
