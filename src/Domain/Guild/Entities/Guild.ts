import { Entity, PrimaryColumn, OneToOne, JoinColumn } from 'typeorm';
import { GuildSetting } from './GuildSetting';

@Entity('guild')
export class Guild {

  @PrimaryColumn('bigint', { nullable: false, unique: true })
  id: number;

  @OneToOne(type => GuildSetting, guildSetting => guildSetting.id)
  @JoinColumn({ name: 'settings_id', referencedColumnName: 'id' })
  settings_id: GuildSetting;
  
}