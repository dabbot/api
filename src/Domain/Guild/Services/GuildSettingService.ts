import { Service } from 'typedi';
import GuildSettingRepository from '../Repositories/GuildSettingRepository';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { Get, JsonController, InternalServerError } from 'routing-controllers';
import { GuildSetting } from '../Entities/GuildSetting';

@Service()
@JsonController("/guildsettings")
export default class GuildSettingService {

  @OrmRepository()
  private readonly guildSettingRepository: GuildSettingRepository;

  @Get("/")
  public all(): Promise<GuildSetting[]> {
    try {
      return this.guildSettingRepository.findAll();
    } catch(e) {
      console.error(e);
      throw new InternalServerError('Could not retrieve guild settings!');
    }
  }
}
