import { Service } from 'typedi';
import GuildRepository from '../Repositories/GuildRepository';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { Get, JsonController, InternalServerError } from 'routing-controllers';
import { Guild } from '../Entities/Guild';

@Service()
@JsonController("/guilds")
export default class GuildService {

  @OrmRepository()
  private readonly guildRepository: GuildRepository;

  @Get("/")
  public all(): Promise<Guild[]> {
    try {
      return this.guildRepository.findAll();
    } catch(e) {
      console.error(e);
      throw new InternalServerError('Could not retrieve guilds!');
    }
  }
}
