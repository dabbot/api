import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('queues')
export class Queue {

  @PrimaryColumn()
  guild_id: number;

  @Column('bigint', { array: true })
  song_ids: number[] = [];
  
}
