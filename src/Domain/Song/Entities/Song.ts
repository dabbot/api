import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('songs')
export class Song {
  
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text', { unique: true, nullable: false })
  track_data: string;
  
}
