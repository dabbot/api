import { Entity, PrimaryColumn, JoinColumn, ManyToMany } from 'typeorm';
import { Guild } from '../../Guild/Entities/Guild';

@Entity('user')
export class User {

  @PrimaryColumn('bigint', { nullable: false, unique: true })
  id: number;

  @ManyToMany(type => Guild, guild => guild.id)
  @JoinColumn({ name: 'guild_ids', referencedColumnName: 'id' })
  guild_ids: Guild[];
  
}