import { Service } from 'typedi';
//import GuildRepository from '../Repositories/GuildRepository';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { Get, JsonController, InternalServerError } from 'routing-controllers';
import { User } from '../Entities/User';
import UserRepository from '../Repositories/UserRepository';

@Service()
@JsonController("/users")
export default class GuildService {

  @OrmRepository()
  private readonly userRepository: UserRepository;

  @Get("/")
  public all(): Promise<User[]> {
    try {
      return this.userRepository.findAll();
    } catch(e) {
      console.error(e);
      throw new InternalServerError('Could not retrieve users!');
    }
  }
}
