import { Service } from 'typedi';
import { EntityRepository } from 'typeorm';
import { AbstractRepository } from 'Abstract/AbstractRepository';
import { User } from '../Entities/User';

@Service()
@EntityRepository(User)
export default class GuildRepository extends AbstractRepository<User> {}
