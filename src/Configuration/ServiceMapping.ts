import GuildSettingService from "../Domain/Guild/Services/GuildSettingService";
import GuildService from "../Domain/Guild/Services/GuildService";

export default [
  GuildService,
  GuildSettingService,
]