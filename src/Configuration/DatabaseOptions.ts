import EntityMapping from "./EntityMapping";

export default {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'postgres',
  database: 'dabbot_develop',
  entities: EntityMapping,
  logging: ['query', 'error'],
  synchronize: true,
}