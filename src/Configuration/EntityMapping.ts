import { GuildSetting } from "../Domain/Guild/Entities/GuildSetting";
import { Queue } from "../Domain/Queue/Entities/Queue";
import { Radio } from "../Domain/Radio/Entities/Radio";
import { RadioCategories } from "../Domain/Radio/Entities/RadioCategories";
import { RadioGenres } from "../Domain/Radio/Entities/RadioGenres";
import { Song } from "../Domain/Song/Entities/Song";

export default [
  GuildSetting,
  Queue,
  Radio,
  RadioCategories,
  RadioGenres,
  Song,
]